# Setting up Portainer Server Docker on CentOS host

> Reference : https://docs.portainer.io/v/ce-2.9/start/install/server/docker/linux

1. Clone the repo to your local storage and make the `setting-up-portainer-server-docker-on-centos-host` directory your current working directory.  
2. Make changes to `prepbook.yml` according to your preferences to reflect the changes in `hostaddr`, `username`, `servlist`, `portsrvr_hostname`, `portsrvr_webp`, `portsrvr_tlsp`, `portsrvr_sock` and `portsrvr_volm` variables.  
3. Once done, execute the following command to populate the primary playbook and inventory file.  
    ```
    ansible-playbook prepbook.yml -vvv
    ```
4. Fetch the `community.docker` collection from Ansible Galaxy by executing the following command.  
    ```
    ansible-galaxy collection install community.docker
    ```
5. Two new files would be generated as a result, so execute the following command to actually start setting up Portainer Server Docker.  
    ```
    ansible-playbook -i register.ini trapplay.yml -vvv
    ```
6. Once the containers are configured and started, open up `https://<ansible_host>:<portsrvr_tlsp/` (or `https://192.168.0.150:9443/` if nothing was changed) using a web browser of your choice on a device connected to the same network as that of the server.
7. After ignoring the certificate warning (as Portainer Server makes use of a self-signed certificate), add the access location of the Portainer Agents (the one that were made note of while setting up Portainer Agent) on the Portainer Server to connect remotely to those environments.
