# Setting up The Lounge Docker on CentOS host

> Reference : https://hub.docker.com/r/linuxserver/thelounge  

1. Clone the repo to your local storage and make the `setting-up-thelounge-docker-on-centos-host` directory your current working directory.  
2. Make changes to `prepbook.yml` according to your preferences to reflect the changes in `hostaddr`, `username`, `servlist`, `lxsvtlng_conf`, `lxsvtlng_hostname`, `lxsvtlng_webp`, and `lxsvtlng_timezone` variables.  
3. Once done, execute the following command to populate the primary playbook and inventory file.  
    ```
    ansible-playbook prepbook.yml -vvv
    ```
4. Fetch the `community.docker` collection from Ansible Galaxy by executing the following command.  
    ```
    ansible-galaxy collection install community.docker
    ```
5. Two new files would be generated as a result, so execute the following command to actually start setting up The Lounge Docker.  
    ```
    ansible-playbook -i register.ini trapplay.yml -vvv
    ```
6. Once the containers are configured and started, open up `http://<ansible_host>:<lxsvtlng_webp>/` (or `http://192.168.0.151:9898/` if nothing was changed) using a web browser of your choice on a device connected to the same network as that of the server.
7. Log in using the account to get started with using The Lounge.  
